PyBox Requirements and Setup
============================
Requirements
------------

The only requirements for using PyBox are a recent and working installation of [Python 2.7.6](http://www.python.org/download/windows/) (generally limited to Python 2.x), the [MSVC++ 2008 Redistributable](https://www.microsoft.com/de-de/download/details.aspx?id=5582#), and a good portion of creativity. :)

Setup
-----

In the following, some general information for setting up PyBox is given. Please read and accept the notice given in the next paragraph. When using PyBox, we suspect that you know what you are doing.

Important Notice
----------------

PyBox can be used for massive manipulations that can reach down to system processes. This can affect the stability of a system significantly. The PyBox framework should therefore not be used on a productive system that is used and required in daily activities. Best use is made on a dedicated analysis system or in a Virtual Machine.

Files
-----

The PyBox framework is ready to run out of the box.
Just put the root folder of the repository somewhere (preconfigured for C:\PyBox) and you are good to go. The stdbox in ./src/boxes/stdbox has an example_notepad.bat file which will startup a notepad.exe process that is being sandboxed with the standard hooking set defined in the other files in ./src/boxes/stdbox. Check the *.bat file to see if you need to make adjustments to the paths.

Current directory structure and short description of the files of which PyBox consists:

* /DLL
* /DLL/PyBox.cpp
* /DLL/PyBox.dll
  * "PyBox.dll" is the readily compiled module (Python 2.7.6), which is being injected to a target process and serves as a platform for the activies of PyBox. 
* /DLL/PyBox275.dll
* /DLL/PyBox276.dll
* /DLL/pydasm275.pyd
* /DLL/pydasm276.pyd
* /src
* /src/starter.py
  * generic example script of a box script. 
* /src/injector.py
  * this script is used to inject a module (for the usage of PyBox, the above introduced PyBox.dll) into a target process. The injector can either start a new process (Path to executable given by parameter -e) or use an already existing process (identified by PID with parameter -p) as target. 
* /src/injector_defines.py
* /src/processrigger.py
* /src/pydasm.pyd
  * pydasm is the python part of the excellent free disassembler libdasm and used by PyBox in the course of installing hooks. 
  * compiled for Python 2.7.6, see /DLL/* for alternative 2.7.5 version.
* /src/pydasm.license
* /src/pybox
  * core files of the PyBox API. 
* /src/pybox/`__init__`.py
* /src/pybox/defines.py
* /src/pybox/dumper.py
* /src/pybox/emodules.py
* /src/pybox/hooking.py
* /src/pybox/memorymanager.py
* /src/pybox/proctrack
* /src/pybox/proctrack/`__init__`.py
  *  module for tracing behaviour of processes in terms of process creation. If it is noticed that a new process is spawned by the monitored process or a new remote thread is started, this module will inject a PyBox into the new process / hosting process for the new remote thread. 
* /src/boxes/stdbox
  *  example box, realizing basic logging sandbox functionality
* /src/boxes/stdbox/example_notepad.bat
* /src/boxes/stdbox/starter.py
* /src/boxes/stdbox/hooks_executable.py
* /src/boxes/stdbox/hooks_file.py
* /src/boxes/stdbox/hooks_memory.py
* /src/boxes/stdbox/hooks_misc.py
* /src/boxes/stdbox/hooks_network.py
* /src/boxes/stdbox/hooks_registry.py
* /src/boxes/stdbox/hooks_services.py
* /src/boxes/stdbox/hooks_synchronisation.py
* /src/boxes/rwxbox
  *  example box, only logging API calls from RWX (=usually injected) memory
* /src/boxes/stdbox/inject_pid.bat
* /src/boxes/stdbox/starter.py
* /src/boxes/stdbox/hooks_memory.py
* /src/boxes/stdbox/hooks_executable.py

Environment Variables
---------------------

* PYTHON: used by Python itself, points to the base installation directory of Python
* PYTHONPATH: used by PyBox, points to PyBox base directory (is set locally as example in stdbox/example_notepad.bat)
* PYBOX\_FILE: used by PyBox, points to the Python starter file used for the desired run (is set locally as example in stdbox/example_notepad.bat)
* PYBOX\_LOG: used by PyBox, points to the directory, in which log files will be generated (is set locally as example in stdbox/example_notepad.bat) 
